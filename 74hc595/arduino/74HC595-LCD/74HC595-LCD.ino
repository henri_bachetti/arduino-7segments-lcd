
#include <TimerOne.h>

#define LATCH_PIN 5
#define CLOCK_PIN 6
#define DATA_PIN  4

#define VI_401_1A           0x0001
#define VI_401_1B           0x0002
#define VI_401_1C           0x0004
#define VI_401_1D           0x0008
#define VI_401_1E           0x0010
#define VI_401_1F           0x0020
#define VI_401_1G           0x0040
#define VI_401_DP           0x0080

uint32_t segTable[10] =
{
  VI_401_1A + VI_401_1B + VI_401_1C + VI_401_1D + VI_401_1E + VI_401_1F,              // 0
  VI_401_1B + VI_401_1C,                                                              // 1
  VI_401_1A + VI_401_1B + VI_401_1G + VI_401_1D + VI_401_1E,                          // 2
  VI_401_1A + VI_401_1B + VI_401_1C + VI_401_1D + VI_401_1G,                          // 3
  VI_401_1B + VI_401_1C + VI_401_1F + VI_401_1G,                                      // 4
  VI_401_1A + VI_401_1C + VI_401_1D + VI_401_1F + VI_401_1G,                          // 5
  VI_401_1A + VI_401_1C + VI_401_1D + VI_401_1E + VI_401_1F + VI_401_1G,              // 6
  VI_401_1A + VI_401_1B + VI_401_1C,                                                  // 7
  VI_401_1A + VI_401_1B + VI_401_1C + VI_401_1D + VI_401_1E + VI_401_1F + VI_401_1G,  // 8
  VI_401_1A + VI_401_1B + VI_401_1C + VI_401_1D + VI_401_1F + VI_401_1G               // 9
};

void setup() {
  Serial.begin(115200);
  Serial.println("74HC595 LCD");
  pinMode(LATCH_PIN, OUTPUT);
  pinMode(CLOCK_PIN, OUTPUT);
  pinMode(DATA_PIN, OUTPUT);
  Timer1.initialize(20000);
  Timer1.attachInterrupt( timerIsr);
}

int counter;

void loop()
{
  static unsigned long previousMillis = 0;
  unsigned long currentMillis = millis();

  if (currentMillis - previousMillis >= 500) {
    previousMillis = currentMillis;
    counter++;
    if (counter > 99) {
      counter = 0;
    }
  }
}

void timerIsr()
{
  static uint8_t cathode = 0;
  uint16_t segs = segs = segTable[counter % 10] << 8;
  segs += segTable[counter / 10];

  Serial.print(counter);
  Serial.print(" ");
  Serial.println(segs, HEX);
  PORTB ^= (1 << PB5);
  cathode = !cathode;

  digitalWrite(LATCH_PIN, LOW);
  if (cathode == 0) {
    // segment bit LO/HI -> pin LO/HI
    shiftOut(DATA_PIN, CLOCK_PIN, MSBFIRST, segs & 0xff);
    shiftOut(DATA_PIN, CLOCK_PIN, MSBFIRST, segs >> 8);
  } else {
    // segment bit LO/HI -> pin HI/LO
    shiftOut(DATA_PIN, CLOCK_PIN, MSBFIRST, ~segs & 0xff);
    shiftOut(DATA_PIN, CLOCK_PIN, MSBFIRST, ~segs >> 8);
  }
  digitalWrite(LATCH_PIN, HIGH);
}

