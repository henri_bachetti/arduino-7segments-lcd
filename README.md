# ARDUINO 7SEGMENTS LCD

The purpose of this page is to explain how to drive 7-segments LCDs with an ARDUINO.

Three solutions are explored :

 * the PCF8577
 * the 74HC595
 * the MCP23017

### ELECTRONICS

The schema is made using KICAD.

### ARDUINO

The code is build using ARDUINO IDE 1.8.5.

### BLOG
A description in french here : https://riton-duino.blogspot.com/2018/09/lcd-tft-et-arduino.html