
#include <Adafruit_MCP23017.h>

#define VI_401_1A           0x0001
#define VI_401_1B           0x0002
#define VI_401_1C           0x0004
#define VI_401_1D           0x0008
#define VI_401_1E           0x0010
#define VI_401_1F           0x0020
#define VI_401_1G           0x0040
#define VI_401_DP           0x0080

uint32_t segTable[10] =
{
  VI_401_1A + VI_401_1B + VI_401_1C + VI_401_1D + VI_401_1E + VI_401_1F,              // 0
  VI_401_1B + VI_401_1C,                                                              // 1
  VI_401_1A + VI_401_1B + VI_401_1G + VI_401_1D + VI_401_1E,                          // 2
  VI_401_1A + VI_401_1B + VI_401_1C + VI_401_1D + VI_401_1G,                          // 3
  VI_401_1B + VI_401_1C + VI_401_1F + VI_401_1G,                                      // 4
  VI_401_1A + VI_401_1C + VI_401_1D + VI_401_1F + VI_401_1G,                          // 5
  VI_401_1A + VI_401_1C + VI_401_1D + VI_401_1E + VI_401_1F + VI_401_1G,              // 6
  VI_401_1A + VI_401_1B + VI_401_1C,                                                  // 7
  VI_401_1A + VI_401_1B + VI_401_1C + VI_401_1D + VI_401_1E + VI_401_1F + VI_401_1G,  // 8
  VI_401_1A + VI_401_1B + VI_401_1C + VI_401_1D + VI_401_1F + VI_401_1G               // 9
};

Adafruit_MCP23017 mcp;

void setup()
{
  Serial.begin(115200);
  Serial.println("MCP23017 LCD");
  mcp.begin();
  Serial.println("MCP23017 OK");
  for (int i = 0 ; i < 16 ; i++) {
    mcp.pinMode(i, OUTPUT);
  }
}

int counter;

void loop()
{
  static unsigned long previousMillis = 0;
  unsigned long currentMillis = millis();

  if (currentMillis - previousMillis >= 1000) {
    // save the last time you blinked the LED
    previousMillis = currentMillis;
    counter++;
    if (counter > 99) {
      counter = 0;
    }
  }
  print(counter);
  delay(10);
}

void print(int counter)
{
  static uint8_t cathode = 0;
  uint16_t segs = segs = segTable[counter % 10] << 8;
  segs += segTable[counter / 10];

  Serial.print(counter);
  Serial.print(" ");
  Serial.println(segs, HEX);
  PORTB ^= (1 << PB5);
  cathode = !cathode;

  if (cathode == 0) {
    // segment bit LO/HI -> pin LO/HI
    mcp.writeGPIOAB(segs);
  } else {
    // segment bit LO/HI -> pin HI/LO
    mcp.writeGPIOAB(~segs);
  }
}

