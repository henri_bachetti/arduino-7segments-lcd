EESchema Schematic File Version 2
LIBS:mcp23017-LCD-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:my-microcontrollers
LIBS:my-misc-modules
LIBS:my-ICs
LIBS:my-displays
LIBS:mcp23017-LCD-cache
EELAYER 25 0
EELAYER END
$Descr A4 8268 11693 portrait
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L arduino_nano U1
U 1 1 5C56E3A6
P 2400 7050
F 0 "U1" H 2700 6000 70  0000 C CNN
F 1 "arduino_nano" H 2700 6250 70  0000 C CNN
F 2 "DIL30" H 2700 7000 60  0000 C CNN
F 3 "" H 2400 7050 60  0000 C CNN
	1    2400 7050
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR2
U 1 1 5C56E5DF
P 1500 8350
F 0 "#PWR2" H 1500 8100 50  0001 C CNN
F 1 "GND" H 1500 8200 50  0000 C CNN
F 2 "" H 1500 8350 50  0000 C CNN
F 3 "" H 1500 8350 50  0000 C CNN
	1    1500 8350
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR1
U 1 1 5C56F23F
P 1500 7650
F 0 "#PWR1" H 1500 7500 50  0001 C CNN
F 1 "+5V" H 1500 7790 50  0000 C CNN
F 2 "" H 1500 7650 50  0000 C CNN
F 3 "" H 1500 7650 50  0000 C CNN
	1    1500 7650
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR4
U 1 1 5C56FCFF
P 4000 8300
F 0 "#PWR4" H 4000 8050 50  0001 C CNN
F 1 "GND" H 4000 8150 50  0000 C CNN
F 2 "" H 4000 8300 50  0000 C CNN
F 3 "" H 4000 8300 50  0000 C CNN
	1    4000 8300
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR3
U 1 1 5C56FEF0
P 4000 7250
F 0 "#PWR3" H 4000 7100 50  0001 C CNN
F 1 "+5V" H 4000 7390 50  0000 C CNN
F 2 "" H 4000 7250 50  0000 C CNN
F 3 "" H 4000 7250 50  0000 C CNN
	1    4000 7250
	1    0    0    -1  
$EndComp
$Comp
L MCP23017 U2
U 1 1 5C571AF3
P 4750 7550
F 0 "U2" H 4800 8450 50  0000 C CNN
F 1 "MCP23017" H 4850 6700 50  0000 C CNN
F 2 "" H 4800 7550 50  0000 C CNN
F 3 "" H 4800 7550 50  0000 C CNN
	1    4750 7550
	-1   0    0    -1  
$EndComp
$Comp
L VI-402-DP U3
U 1 1 5C586507
P 2500 2200
F 0 "U3" H 4500 1150 60  0000 C CNN
F 1 "VI-402-DP" H 4000 1150 60  0000 C CNN
F 2 "" H 4950 -200 60  0000 C CNN
F 3 "" H 4950 -200 60  0000 C CNN
	1    2500 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 2400 4700 2700
Wire Wire Line
	6250 2400 4700 2400
Wire Wire Line
	4500 5600 4500 5250
Wire Wire Line
	6200 5600 4500 5600
Wire Wire Line
	4600 5650 4600 5250
Wire Wire Line
	6150 5650 4600 5650
Wire Wire Line
	4700 5700 4700 5250
Wire Wire Line
	6100 5700 4700 5700
Wire Wire Line
	4900 2450 4900 2700
Wire Wire Line
	6050 2450 4900 2450
Wire Wire Line
	4800 2500 4800 2700
Wire Wire Line
	6000 2500 4800 2500
Wire Wire Line
	4500 2200 4500 2700
Wire Wire Line
	5950 2200 4500 2200
Wire Wire Line
	5000 2250 5000 2700
Wire Wire Line
	5900 2250 5000 2250
Wire Wire Line
	5100 2300 5100 2700
Wire Wire Line
	5850 2300 5100 2300
Wire Wire Line
	4900 5400 4900 5250
Wire Wire Line
	5800 5400 4900 5400
Wire Wire Line
	5000 5450 5000 5250
Wire Wire Line
	5750 5450 5000 5450
Wire Wire Line
	5100 5500 5100 5250
Wire Wire Line
	5700 5500 5100 5500
Wire Wire Line
	5200 5300 5200 5250
Wire Wire Line
	5650 5300 5200 5300
Wire Wire Line
	5200 2550 5200 2700
Wire Wire Line
	5600 2550 5200 2550
Wire Wire Line
	5950 2200 5950 7450
Wire Wire Line
	5950 7450 5400 7450
Wire Wire Line
	5900 7350 5900 2250
Wire Wire Line
	5400 7350 5900 7350
Wire Wire Line
	5850 7250 5850 2300
Wire Wire Line
	5400 7250 5850 7250
Wire Wire Line
	5800 7150 5800 5400
Wire Wire Line
	5400 7150 5800 7150
Wire Wire Line
	5750 7050 5750 5450
Wire Wire Line
	5400 7050 5750 7050
Wire Wire Line
	5700 6950 5400 6950
Wire Wire Line
	5700 5500 5700 6950
Wire Wire Line
	5650 6850 5400 6850
Wire Wire Line
	5650 5300 5650 6850
Wire Wire Line
	5600 2550 5600 6750
Wire Wire Line
	5600 6750 5400 6750
Wire Wire Line
	6350 8250 6350 5550
Wire Wire Line
	5400 8250 6350 8250
Wire Wire Line
	6300 8150 6300 2350
Wire Wire Line
	5400 8150 6300 8150
Wire Wire Line
	6250 8050 5400 8050
Wire Wire Line
	6250 2400 6250 8050
Wire Wire Line
	6200 7950 6200 5600
Wire Wire Line
	5400 7950 6200 7950
Wire Wire Line
	6150 7850 6150 5650
Wire Wire Line
	5400 7850 6150 7850
Wire Wire Line
	6100 7750 6100 5700
Wire Wire Line
	5400 7750 6100 7750
Wire Wire Line
	6050 7650 6050 2450
Wire Wire Line
	5400 7650 6050 7650
Wire Wire Line
	6000 2500 6000 7550
Wire Wire Line
	6000 7550 5400 7550
Connection ~ 3000 5700
Wire Wire Line
	3300 5700 3300 5250
Wire Wire Line
	1600 6200 1700 6200
Wire Wire Line
	1600 5700 1600 6200
Wire Wire Line
	1600 5700 3300 5700
Wire Wire Line
	3000 2600 3000 5700
Wire Wire Line
	3300 2600 3000 2600
Wire Wire Line
	3300 2700 3300 2600
Connection ~ 4000 8200
Wire Wire Line
	4000 8200 4100 8200
Connection ~ 4000 8100
Wire Wire Line
	4100 8100 4000 8100
Wire Wire Line
	4000 8000 4100 8000
Wire Wire Line
	4000 8000 4000 8300
Wire Wire Line
	4000 7350 4100 7350
Wire Wire Line
	4000 7250 4000 7350
Wire Wire Line
	4650 5850 4650 6400
Wire Wire Line
	1450 5850 4650 5850
Wire Wire Line
	1450 7400 1450 5850
Wire Wire Line
	1700 7400 1450 7400
Wire Wire Line
	1500 7250 1700 7250
Wire Wire Line
	1500 5950 1500 7250
Wire Wire Line
	4550 5950 1500 5950
Wire Wire Line
	4550 6400 4550 5950
Wire Wire Line
	1500 7850 1700 7850
Wire Wire Line
	1500 7650 1500 7850
Wire Wire Line
	1500 8150 1700 8150
Wire Wire Line
	1500 8350 1500 8150
Wire Wire Line
	6350 5550 4800 5550
Wire Wire Line
	4800 5550 4800 5250
Wire Wire Line
	6300 2350 4600 2350
Wire Wire Line
	4600 2350 4600 2700
$EndSCHEMATC
