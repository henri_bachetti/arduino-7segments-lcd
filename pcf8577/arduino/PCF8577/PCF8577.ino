
#include <Wire.h>

#define PCF8577_ADDR        0x3A
#define PCF8577_S1          0x00000001L
#define PCF8577_S2          0x00000002L
#define PCF8577_S3          0x00000004L
#define PCF8577_S4          0x00000008L
#define PCF8577_S5          0x00000010L
#define PCF8577_S6          0x00000020L
#define PCF8577_S7          0x00000040L
#define PCF8577_S8          0x00000080L
#define PCF8577_S9          0x00000100L
#define PCF8577_S10         0x00000200L
#define PCF8577_S11         0x00000400L
#define PCF8577_S12         0x00000800L
#define PCF8577_S13         0x00001000L
#define PCF8577_S14         0x00002000L
#define PCF8577_S15         0x00004000L
#define PCF8577_S16         0x00008000L
#define PCF8577_S17         0x00010000L
#define PCF8577_S18         0x00020000L
#define PCF8577_S19         0x00040000L
#define PCF8577_S20         0x00080000L
#define PCF8577_S21         0x00100000L
#define PCF8577_S22         0x00200000L
#define PCF8577_S23         0x00400000L
#define PCF8577_S24         0x00800000L
#define PCF8577_S25         0x01000000L
#define PCF8577_S26         0x02000000L
#define PCF8577_S27         0x04000000L
#define PCF8577_S28         0x08000000L
#define PCF8577_S29         0x10000000L
#define PCF8577_S30         0x20000000L
#define PCF8577_S31         0x40000000L
#define PCF8577_S32         0x80000000L

#define NDIGITS             4
#define VI_401_DP1          PCF8577_S9
#define VI_401_DP2          PCF8577_S5
#define VI_401_DP3          PCF8577_S1
#define VI_401_COL          PCF8577_S21
#define VI_401_1A           PCF8577_S15
#define VI_401_1B           PCF8577_S16
#define VI_401_1C           PCF8577_S10
#define VI_401_1D           PCF8577_S11
#define VI_401_1E           PCF8577_S12
#define VI_401_1F           PCF8577_S14
#define VI_401_1G           PCF8577_S13
#define VI_401_2A           PCF8577_S19
#define VI_401_2B           PCF8577_S20
#define VI_401_2C           PCF8577_S6
#define VI_401_2D           PCF8577_S7
#define VI_401_2E           PCF8577_S8
#define VI_401_2F           PCF8577_S18
#define VI_401_2G           PCF8577_S17
#define VI_401_3A           PCF8577_S24
#define VI_401_3B           PCF8577_S25
#define VI_401_3C           PCF8577_S2
#define VI_401_3D           PCF8577_S3
#define VI_401_3E           PCF8577_S4
#define VI_401_3F           PCF8577_S23
#define VI_401_3G           PCF8577_S22
#define VI_401_4A           PCF8577_S28
#define VI_401_4B           PCF8577_S32
#define VI_401_4C           PCF8577_S31
#define VI_401_4D           PCF8577_S30
#define VI_401_4E           PCF8577_S29
#define VI_401_4F           PCF8577_S27
#define VI_401_4G           PCF8577_S26

uint32_t segTable[4][10] =
{
  {
    VI_401_1A + VI_401_1B + VI_401_1C + VI_401_1D + VI_401_1E + VI_401_1F,              // 0
    VI_401_1B + VI_401_1C,                                                              // 1
    VI_401_1A + VI_401_1B + VI_401_1G + VI_401_1D + VI_401_1E,                          // 2
    VI_401_1A + VI_401_1B + VI_401_1C + VI_401_1D + VI_401_1G,                          // 3
    VI_401_1B + VI_401_1C + VI_401_1F + VI_401_1G,                                      // 4
    VI_401_1A + VI_401_1C + VI_401_1D + VI_401_1F + VI_401_1G,                          // 5
    VI_401_1A + VI_401_1C + VI_401_1D + VI_401_1E + VI_401_1F + VI_401_1G,              // 6
    VI_401_1A + VI_401_1B + VI_401_1C,                                                  // 7
    VI_401_1A + VI_401_1B + VI_401_1C + VI_401_1D + VI_401_1E + VI_401_1F + VI_401_1G,  // 8
    VI_401_1A + VI_401_1B + VI_401_1C + VI_401_1D + VI_401_1F + VI_401_1G               // 9
  },
  {
    VI_401_2A + VI_401_2B + VI_401_2C + VI_401_2D + VI_401_2E + VI_401_2F,              // 0
    VI_401_2B + VI_401_2C,                                                              // 1
    VI_401_2A + VI_401_2B + VI_401_2G + VI_401_2D + VI_401_2E,                          // 2
    VI_401_2A + VI_401_2B + VI_401_2C + VI_401_2D + VI_401_2G,                          // 3
    VI_401_2B + VI_401_2C + VI_401_2F + VI_401_2G,                                      // 4
    VI_401_2A + VI_401_2C + VI_401_2D + VI_401_2F + VI_401_2G,                          // 5
    VI_401_2A + VI_401_2C + VI_401_2D + VI_401_2E + VI_401_2F + VI_401_2G,              // 6
    VI_401_2A + VI_401_2B + VI_401_2C,                                                  // 7
    VI_401_2A + VI_401_2B + VI_401_2C + VI_401_2D + VI_401_2E + VI_401_2F + VI_401_2G,  // 8
    VI_401_2A + VI_401_2B + VI_401_2C + VI_401_2D + VI_401_2F + VI_401_2G               // 9
  },
  {
    VI_401_3A + VI_401_3B + VI_401_3C + VI_401_3D + VI_401_3E + VI_401_3F,              // 0
    VI_401_3B + VI_401_3C,                                                              // 1
    VI_401_3A + VI_401_3B + VI_401_3G + VI_401_3D + VI_401_3E,                          // 2
    VI_401_3A + VI_401_3B + VI_401_3C + VI_401_3D + VI_401_3G,                          // 3
    VI_401_3B + VI_401_3C + VI_401_3F + VI_401_3G,                                      // 4
    VI_401_3A + VI_401_3C + VI_401_3D + VI_401_3F + VI_401_3G,                          // 5
    VI_401_3A + VI_401_3C + VI_401_3D + VI_401_3E + VI_401_3F + VI_401_3G,              // 6
    VI_401_3A + VI_401_3B + VI_401_3C,                                                  // 7
    VI_401_3A + VI_401_3B + VI_401_3C + VI_401_3D + VI_401_3E + VI_401_3F + VI_401_3G,  // 8
    VI_401_3A + VI_401_3B + VI_401_3C + VI_401_3D + VI_401_3F + VI_401_3G               // 9
  },
  {
    VI_401_4A + VI_401_4B + VI_401_4C + VI_401_4D + VI_401_4E + VI_401_4F,              // 0
    VI_401_4B + VI_401_4C,                                                              // 1
    VI_401_4A + VI_401_4B + VI_401_4G + VI_401_4D + VI_401_4E,                          // 2
    VI_401_4A + VI_401_4B + VI_401_4C + VI_401_4D + VI_401_4G,                          // 3
    VI_401_4B + VI_401_4C + VI_401_4F + VI_401_4G,                                      // 4
    VI_401_4A + VI_401_4C + VI_401_4D + VI_401_4F + VI_401_4G,                          // 5
    VI_401_4A + VI_401_4C + VI_401_4D + VI_401_4E + VI_401_4F + VI_401_4G,              // 6
    VI_401_4A + VI_401_4B + VI_401_4C,                                                  // 7
    VI_401_4A + VI_401_4B + VI_401_4C + VI_401_4D + VI_401_4E + VI_401_4F + VI_401_4G,  // 8
    VI_401_4A + VI_401_4B + VI_401_4C + VI_401_4D + VI_401_4F + VI_401_4G               // 9
  }
};

// 1111 = 
uint8_t dotTable[] =
{
  //0     1     2
  0x40, 0x06, 0x40
};

int PCF8577_write(uint8_t *buf, int len)
{
  digitalWrite(13, HIGH);
  Wire.beginTransmission(PCF8577_ADDR);
  for (int i = 0 ; i < len ; i++) {
    Wire.write(buf[i]);
  }
  int error = Wire.endTransmission();
  digitalWrite(13, LOW);
  return error;
}

struct regs
{
  uint8_t cr;
  uint32_t sr;
};

#define htonl(x) ( ((x)<<24 & 0xFF000000UL) | \
                   ((x)<< 8 & 0x00FF0000UL) | \
                   ((x)>> 8 & 0x0000FF00UL) | \
                   ((x)>>24 & 0x000000FFUL) )

int PCF8577_displayInt(int i, int col=0, int dot=0)
{
  uint32_t digit[NDIGITS];
  struct regs r;

  Serial.print("PCF8577_displayInt: ");
  Serial.println(i);
  if (i > 9999) {
    return PCF8577_displayOverflow();
  }
  digit[0] = segTable[0][i / 1000];
  digit[1] = segTable[1][i % 1000 / 100];
  digit[2] = segTable[2][i % 100 / 10];
  digit[3] = segTable[3][i % 10];
  r.cr = 0;
  r.sr = digit[0] | digit[1] | digit[2] | digit[3];
  if (col) {
    r.sr |= VI_401_COL;
  }
  if (dot == 1) {
    r.sr |= VI_401_DP1;
  }
  if (dot == 2) {
    r.sr |= VI_401_DP2;
  }
  if (dot == 3) {
    r.sr |= VI_401_DP3;
  }
  return PCF8577_write((uint8_t *)&r, sizeof(r));
}

int PCF8577_displayOverflow(void)
{
  struct regs r;
  
  r.cr = 0;
  r.sr = VI_401_1D | VI_401_2D | VI_401_3D | VI_401_4D;
  return PCF8577_write((uint8_t *)&r, sizeof(r));
}

int PCF8577_displayHour(int hour, int min)
{
  Serial.print("PCF8577_displayHour: ");
  Serial.print(hour);
  Serial.print(":");
  Serial.println(min);
  return PCF8577_displayInt(hour*100+min, 1);
}

int PCF8577_displayFloat(float n)
{
  char s[10];

  Serial.print("PCF8577_displayFloat: ");
  Serial.println(n, NDIGITS);
  if (n > 9999) {
    return PCF8577_displayOverflow();
  }
  dtostrf(n, NDIGITS+1, NDIGITS, s);
  Serial.print("s: ");
  Serial.println(s);
  char *p = strchr(s, '.');
  int dp = p ? p - s : 0;
  strcpy(p, p+1);
  s[4] = 0;
  Serial.print("dp: ");
  Serial.println(dp);
  return PCF8577_displayInt(atoi(s), 0, dp);
}

int PCF8577_clear(void)
{
  struct regs r;
  
  r.cr = 0;
  r.sr = 0;
  return PCF8577_write((uint8_t *)&r, sizeof(r));
}

void setup() {
  pinMode(13, OUTPUT);
  Wire.begin();
  Serial.begin(115200);
}

void loop()
{
  int error;
  error = PCF8577_clear();
  Serial.print("PCF8577_clear: ");
  Serial.println(error);
  delay(1000);
  error = PCF8577_displayHour(8, 23);
  Serial.print("error: ");
  Serial.println(error);
  delay(1000);
  error = PCF8577_displayInt(10000);
  Serial.print("error: ");
  Serial.println(error);
  delay(1000);
  for (int i = 0 ; i < 10 ; i++) {
    int n = i*1000 + i*100 + i*10 + i;
    error = PCF8577_displayInt(n);
    Serial.print("error: ");
    Serial.println(error);
    delay(200);
  }
  float n = 1234;
  error = PCF8577_displayFloat(n);
  Serial.print("error: ");
  Serial.println(error);
  delay(1000);
  n = 123.4;
  error = PCF8577_displayFloat(n);
  Serial.print("error: ");
  Serial.println(error);
  delay(1000);
  n = 12.34;
  error = PCF8577_displayFloat(n);
  Serial.print("error: ");
  Serial.println(error);
  delay(1000);
  n = 1.234;
  error = PCF8577_displayFloat(n);
  Serial.print("error: ");
  Serial.println(error);
  delay(1000);
  n = 0.1234;
  error = PCF8577_displayFloat(n);
  Serial.print("error: ");
  Serial.println(error);
  delay(1000);
  n = 0.01234;
  error = PCF8577_displayFloat(n);
  Serial.print("error: ");
  Serial.println(error);
  delay(1000);
  n = 0.001234;
  error = PCF8577_displayFloat(n);
  Serial.print("error: ");
  Serial.println(error);
  delay(1000);
  n = 0.0001234;
  error = PCF8577_displayFloat(n);
  Serial.print("error: ");
  Serial.println(error);
  delay(1000);
}

